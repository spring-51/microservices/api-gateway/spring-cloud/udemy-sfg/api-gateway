package com.gateway.api.enums;


public enum AppPath {
    GOOGLE(null,"/googlesearch2", "https://google.com"),
    USER_SERVICE(null,"/user-service/**", "http://127.0.0.1:8181"),
    TOKEN_SERVICE(null,"/token-service/**", "http://127.0.0.1:8182")
    ;

    /*
    NOT NEEDED in spring boot 2.7.6
     */
    public final String ID;
    public final String PATH;
    public final String URI;


    AppPath(String id, String path, String uri) {
        this.ID = id;
        this.PATH = path;
        this.URI = uri;
    }

}


