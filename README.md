# Spring Cloud API - Gateway

#### Tutor
```
udemy/springframeworkguru/Spring Boot Micreoservices With Spring


```

#### Postman Collection

```
refer - ./postman/api-gateway.postman_collection.json
      
```

## Section 19 - Spring Cloud Gateway

#### Java Based Route Configuration

##### Important File

```


1. GoogleConfig
  - GoogleConfig -> configRoutes(RouteLocatorBuilder)
  - here we used hava based route confuguration
```

## Important Links

```
1. AntPathMAtcher Expression
https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/util/AntPathMatcher.html
```