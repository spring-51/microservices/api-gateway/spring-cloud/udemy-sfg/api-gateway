package com.gateway.api.config;

import com.gateway.api.constants.AppProfile;
import com.gateway.api.enums.AppPath;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile(value = {AppProfile.LOCALHOST})
@Configuration
public class LocalHostRouteConfig {

    @Bean
    public RouteLocator localHostRoutes(RouteLocatorBuilder builder){
        return builder
                .routes()
                .route(p -> p.path(AppPath.USER_SERVICE.PATH).uri(AppPath.USER_SERVICE.URI))

                .route(p -> p.path(AppPath.TOKEN_SERVICE.PATH).uri(AppPath.TOKEN_SERVICE.URI))
                .build()
                ;
    }
}
