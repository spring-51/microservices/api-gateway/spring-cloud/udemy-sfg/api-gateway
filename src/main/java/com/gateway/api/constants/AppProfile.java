package com.gateway.api.constants;

public class AppProfile {
    public final static String GOOGLE = "google";
    public final static String LOCALHOST = "local";
}
