package com.gateway.api.config;

import com.gateway.api.constants.AppProfile;
import com.gateway.api.enums.AppPath;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile(
        value = {AppProfile.GOOGLE}
        )
@Configuration
public class GoogleRouteConfig {

    @Bean
    public RouteLocator configRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(p -> p
                        .path(AppPath.GOOGLE.PATH)
                         // WITHOUT the below rewritePath athe redirection result 404
                        .filters(f -> f.rewritePath(AppPath.GOOGLE.PATH+ "(?<segment>/?.*)", "/${segment}"))
                        .uri(AppPath.GOOGLE.URI)
                )
                .build();
    }
}
